# Ubuntu LTS Ansible Test Image

| **Prod** | **Dev** |
| :---: | :---: |
| [![pipeline status](https://gitlab.com/ninuxorg/docker/ansible-ubuntu/badges/master/pipeline.svg)](https://gitlab.com/ninuxorg/docker/ansible-ubuntu/commits/master) | [![pipeline status](https://gitlab.com/ninuxorg/docker/ansible-ubuntu/badges/develop/pipeline.svg)](https://gitlab.com/ninuxorg/docker/ansible-ubuntu/commits/develop)|

Ubuntu LTS Docker containers for Ansible playbook and role testing.

## How to Use

  1. [Install Docker](https://docs.docker.com/engine/installation/).
  2. Pull this image from Ninux Gitlab Docker Registry:
  
    ```bash
    # Ubuntu 14.04
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-ubuntu:14.04
    
    #Ubuntu 16.04
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-ubuntu:16.04

    #Ubuntu 18.04
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-ubuntu:18.04
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-ubuntu:latest
    ```
   
  3. Run a container from the image:
    
    ```bash
    # Ubuntu 14.04
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-ubuntu:14.04 /sbin/init
    
    # Ubuntu 16.04
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-ubuntu:16.04 /usr/lib/systemd/systemd

    # Ubuntu 18.04
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-ubuntu:18.04 /usr/lib/systemd/systemd
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-ubuntu:latest /usr/lib/systemd/systemd
    ```

  4. Use Ansible inside the container:
    
    ```bash
    docker exec --tty [container_id] env TERM=xterm ansible --version
    docker exec --tty [container_id] env TERM=xterm ansible-playbook /path/to/ansible/playbook.yml --syntax-check
    ```
    
